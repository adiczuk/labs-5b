﻿using System;
using PK.Container;
using Główny.Implementacja;
using Główny.Kontrakt;
using Wyświetlacz.Kontrakt;
using Wyświetlacz.Implementacja;
using Lab5.DisplayForm;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer kontener = new Container();

            IWyswietlacz wyswietlacz = new Wyswietlacz(new DisplayViewModel());
            IGlowny glowny = new Glowny(wyswietlacz);

            kontener.Register(wyswietlacz);
            kontener.Register(glowny);

            return kontener;
        }
    }
}
