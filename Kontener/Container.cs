﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PK.Container;

namespace PK.Container
{
    public class Container : IContainer
    {
        private IDictionary<Type, Type> interfejsy; // bez implementacji
        private IDictionary<Type, object> obiekty; // z implementacja

        public Container()
        {
            interfejsy = new Dictionary<Type, Type>();
            obiekty = new Dictionary<Type, object>();
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (Type typ in assembly.GetTypes()) //pobieramy typy z assembly
            {
                Register(typ);
            }
        }

        public void Register(Type type)
        {
            foreach (Type typInterfejs in type.GetInterfaces()) // interfejsy bez implementacji
            {
                if (interfejsy.Keys.Contains(typInterfejs))
                    interfejsy[typInterfejs] = type;
                else
                    interfejsy.Add(typInterfejs, type);
            }
        }

        public void Register<T>(T impl) where T : class
        {
            foreach (Type typInterfejs in impl.GetType().GetInterfaces()) // interfejsy z implementacja
            {
                if (obiekty.Keys.Contains(typInterfejs))
                    obiekty[typInterfejs] = impl;
                else
                    obiekty.Add(typInterfejs, impl);
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            T result = provider.Invoke();

            Register(result);
        }

        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }

        public object Resolve(Type type)
        {
            if (obiekty.Keys.Contains(type))
                return obiekty[type];
            if (!interfejsy.Keys.Contains(type))
                return null;

            List<object> listaParametrow = new List<object>();
            foreach (ConstructorInfo ctorInfo in interfejsy[type].GetConstructors())
            {
                foreach (ParameterInfo paramInfo in ctorInfo.GetParameters())
                {
                    if (interfejsy.Keys.Contains(paramInfo.ParameterType))
                        listaParametrow.Add(Activator.CreateInstance(interfejsy[paramInfo.ParameterType]));
                    else
                        throw new UnresolvedDependenciesException();
                }
            }
            return Activator.CreateInstance(interfejsy[type], listaParametrow.ToArray()); // lista nie przeszła

        }
    }
}